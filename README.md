# flask-project


## Test Python

En utilisant Flask, créer un swagger api avec trois routes :\
1/ ‘health’ : Vérifier que l’api tourne.\
2/ ‘longestCommonStreak’ : Détecter pour deux mots donnés, la plus longue séquence qu’ils ont en commun. 
Exemple : nous avons les mots A = pythontesting, B = pythontestfunction, C = python et D = bye.\
Donc entre :\
A et B => pythontest\
A et C => python\
A et D => y\
3/ ‘emailValidation’ : Ecrit un algorithme qui permet de vérifier si une adresse email est valide ou non. La fonction retournera False si l’email est invalide et True s’il est valide.
