# main.py
from flasgger import Swagger
import re
from difflib import SequenceMatcher
from flask import Flask,jsonify
app = Flask(__name__)
swagger = Swagger(app)

def find_longest_match_between_two_stings(first_string,second_string):
    match = SequenceMatcher(None, first_string,
                        second_string).find_longest_match(0, len(first_string), 0,
                                                    len(second_string))
    return first_string[match.a:match.a + match.size] # or second_string[match.b:match.b + match.size]

def check_email_validity(s):
   pat = "^[a-zA-Z0-9-]+@[a-zA-Z0-9]+.[a-z]{1,3}$"
   if re.match(pat,s):
      return True
   return False

@app.route('/basic_api/health')
def health():
    return 'server is running'

@app.route('/basic_api/longestCommonStreak/<string_1>/<string_2>')
def longestCommonStreak(string_1,string_2):
    """Longest match between two strings
        ---
        parameters:
          - name: string_1
            in: path
            type: string
            required: true

          - name: string_2
            in: path
            type: string
            required: true


        definitions:
          string_1:
            type: str

          string_2:
            type: str
        responses:
          200:
            description: Succes
            schema:
                type: str
                example: "pythonstring"


        """

    find_longest_match_between_two_stings(string_1,string_2)
    result =find_longest_match_between_two_stings(string_1,string_2)
    print(result)

    return result

@app.route('/basic_api/emailValidation/<email>')
def emailValidation(email):
    """Email format verification
    ---
    parameters:
      - name: email
        in: path
        description: email adresse
        type: string
        required: true
    definitions:
        email:
            type: str
    responses:
      200:
        description: Success
        schema:
            type: str
            example: mail@mail.com

    """
    return jsonify(check_email_validity(email))



